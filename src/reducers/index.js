import LanguageReducer from './LanguageReducer';
import AuthReducer from './AuthReducer';
import MenuReducer from './MenuReducer'
import { combineReducers } from "redux";


export default combineReducers({
    lang: LanguageReducer,
    auth: AuthReducer,
    menu: MenuReducer,
});