
import React, { Component } from 'react';
import './nav.css';
import {Icon} from 'react-materialize';
import { Dropdown,Menu,Form} from 'antd';
import "antd/dist/antd.css";
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {logout} from '../../actions/LogoutActions'
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'




class Nav extends Component {
  
    state = {
     
    }

    constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    componentDidMount(){
 
    }
    

  render() {
    const {logout, currentUser, userToken,history} = this.props;
    //console.log(this.props.isRTL)

        const menu2 = (
          <Menu>
            <Menu.Item
            onClick={()=> this.props.history.push('/AdminInfo',{data:this.props.currentUser.user})}
             key="0">{allStrings.profile}</Menu.Item>
            <Menu.Divider />
             {/*<Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(true)
              allStrings.setLanguage('ar')
              localStorage.setItem('@lang','ar'); 
            }}
             key="2">{allStrings.arabic}</Menu.Item>
             <Menu.Item 
            onClick={()=>{
              this.props.ChangeLanguage(false)
              allStrings.setLanguage('en')
              localStorage.setItem('@lang','en');
            }}
          key="3">{allStrings.english}</Menu.Item> {*/}
              <Menu.Item 
            onClick={()=>logout(userToken, currentUser.token, history)}
             key="1">{allStrings.logout}</Menu.Item>
          </Menu>
        );
    
    return (
      <nav>
        
      
        <div className={`nav-wrapper ${!this.props.select&&'nav-wrapper-active'}`}>
          <ul className="right hide-on-med-and-down">
            <li className='action'>
            <Dropdown overlay={menu2} trigger={['click']}>
              <a className="ant-dropdown-link" href="#">
              <Icon>more_vert</Icon>
              </a>
            </Dropdown>
            
            </li>
          </ul>
        </div>
      </nav> 
    );
  }
}

const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
  select: state.menu.select
})

const mapDispatchToProps = {
  logout,
  ChangeLanguage
}

export default withRouter(connect(mapToStateProps,mapDispatchToProps)(Nav = Form.create({ name: 'normal_login' })(Nav)));

