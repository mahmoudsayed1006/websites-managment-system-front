import React, { Component } from 'react';
import './menu.css';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'
import { Menu, Icon} from 'antd';
import "antd/dist/antd.css";
import {SelectMenuItem } from '../../actions/MenuAction'

const { SubMenu } = Menu;

class AppMenu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
     const {goTo,height} = this.props;
     console.log("GO TO   ",goTo)
    const x  = !this.props.select;
    return (
      <div style={{direction:"rtl"}} >
    
         <Menu  
         style={{position:'fixed',background:'#3497fd',top:0}}       
          //defaultSelectedKeys={[this.props.key]}
          //defaultOpenKeys={['sub1']}   
          mode="inline"
          theme="dark"
          inlineCollapsed={this.props.select}
        >
          <Menu.Item  style={{marginTop: "14px",marginBottom:26}}
              onClick={()=>{
               
              this.props.SelectMenuItem(0,x)
              }} >
              <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
              
          </Menu.Item>

          <Menu.Item
            style={{}}
            onClick={()=>{
              goTo.push('/Dashboard')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem(1,true)
            }}
            key="1" style={{marginBottom:26}}>
            <Icon type="home"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.home}</span>
          </Menu.Item>
          <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/users')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(2,true)
              }}
              key="2" style={{marginBottom:26}}>
              <Icon type="usergroup-add" style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.users}</span>
          </Menu.Item>
          <Menu.Item
            onClick={()=>{
              goTo.push('/screens')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('20',true)
            }}
            key="33" style={{marginBottom:26}}> 
                  
            <Icon type="desktop"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.screens}</span>
          </Menu.Item>
          <Menu.Item
            onClick={()=>{
              goTo.push('/Youtube')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('20',true)
            }}
            key="38" style={{marginBottom:26}}> 
                  
            <Icon type="play-circle"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.youtube}</span>
          </Menu.Item>
          <Menu.Item
            onClick={()=>{
              goTo.push('/Website')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('20',true)
            }}
            key="322" style={{marginBottom:26}}> 
                  
            <Icon type="ie"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.websites}</span>
          </Menu.Item>
          <Menu.Item
            onClick={()=>{
              goTo.push('/Department')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('20',true)
            }}
            key="322" style={{marginBottom:43}}> 
                  
            <Icon type="appstore"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.department}</span>
          </Menu.Item>
        </Menu>

      </div>

    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  key: state.menu.key,
  select: state.menu.select
})

const mapDispatchToProps = {
  SelectMenuItem,
}

export default connect(mapToStateProps,mapDispatchToProps) (AppMenu);
