import { message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {
  LOGIN_REQUEST,LOGIN_SUCCESS,USER_TOKEN,CURRENT_USER
   } from './types';
import { BASE_END_POINT } from '../config/URL';


//

export function login(user, FB_token, history) {
  // console.log(FB_token)
   return (dispatch) => {    
     dispatch({ type: LOGIN_REQUEST });
     let l = message.loading('Wait..', 2.5)
     axios.post(`${BASE_END_POINT}signin`, JSON.stringify(user), {
       headers: {
         'Content-Type': 'application/json',
       },
     }).then(res => {
       l.then(() => message.success('Welcome', 2.5))
      // window.location.href = 'https://www.google.com'; 
       console.log('login Done  ',res.data);   
       if(res.data.user.type ==='ADMIN'){
          localStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
          dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
          history.push('/Dashboard')    
       } else{
        localStorage.setItem('@QsathaUser', JSON.stringify(res.data));  
        dispatch({ type: LOGIN_SUCCESS, payload: res.data});  
        history.push('/Show')  
        //window.location.href = res.data.webSite;
       }
       
     
     })
       .catch(error => {
         l.then(() => message.error('Email Or Password is incorrect', 2.5))
         //console.log('outer');
         //console.log(error.response);
       });
   };
 }
 

export function getUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}


export function userToken(token){
  //console.log("hi  "+token)
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}
