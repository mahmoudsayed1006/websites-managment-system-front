import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';

import './website.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Website extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedWebsite:null,
     Websites:[],
     departments:[],
     file:null,
     loading:true,
     tablePage:0,
     flagInterval: 0
     }

     constructor(props){
      super(props)
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    getDepartments = () => {
      axios.get(`${BASE_END_POINT}department/get`)
      .then(response=>{
        console.log(response.data.data)
        this.setState({departments:response.data.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
    }
    showIntervals = () => {
      const flagInterval = 1;
      this.setState({ flagInterval });
    }

    //submit form
    flag = -1;
    getWebsites = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}website/get?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Websites    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({Websites:deleteRow?response.data.data:[...this.state.Websites,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL Websites ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getWebsites(1,true)
      this.getDepartments()
    }
    OKBUTTON = (e) => {
      this.deleteWebsite()
     }

     deleteWebsite = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}website/${this.state.selectedWebsite}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getWebsites(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('date ', date[2]  );
          const data = {
              link: values.link,
              title:values.title,
              department:values.department.key
              
          }
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}website`,data,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log(allStrings.addDone)
              l.then(() => message.success('Add Website', 2.5));
              this.setState({ modal1Visible:false });
              this.getWebsites(1,true)
              this.flag = -1
              this.props.form.resetFields() 
          })
          .catch(error=>{
            console.log("Add Website Error")
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })

        }
      });
      
    }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
      const Option = Select.Option;
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list =this.state.Websites.map((val,index)=>[
          val.id,
          val.link,
          val.title,
          val.department.name,
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
           const { flagInterval} = this.state;
      return (
          <div style={{paddingBottom: '100px'}}>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <div style={{height:' 200px',padding: '25px 50px'}}>
                <img style={{width:'250px',height: '150px',borderRadius: '0px'}} src="https://res.cloudinary.com/boody-car/image/upload/v1589791155/s0ovt3jakmi8cive0c7p.png">
                </img>
              </div>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.link,allStrings.title,allStrings.department,allStrings.remove]} 
              title={allStrings.websites}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==4){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/WebsiteInfo',{data:this.state.Websites[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===4){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedWebsite:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getWebsites(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addWebsite}</Button>
              <Modal
                    title={allStrings.addWebsite}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                       
                        <Form.Item>
                        {getFieldDecorator('link', {
                            rules: [{ required: true, message: 'Please enter link' }],
                        })(
                            <Input placeholder={allStrings.link}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please enter display name' }],
                        })(
                            <Input placeholder={allStrings.title}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('department', {
                            rules: [{ required: true, message: 'Please enter department' }],
                        })(
                          <Select labelInValue  
                          placeholder={allStrings.department}
                          style={{ width: '100%'}} >
                              {this.state.departments.map(val=>
                              <Option value={val.id}>{val.name}</Option>
                              )}                 
                          </Select>
                        )}
                        </Form.Item>
                     
                       
                    </Form>
                </Modal>
              </div> 
            </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Website = Form.create({ name: 'normal_login' })(Website));
