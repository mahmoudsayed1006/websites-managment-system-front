import React, { Component } from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';

import './admin info.css';
import {Input} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select,message} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {getUser} from '../../actions/AuthActions'



class AdminInfo extends React.Component {
      page=1;
      type=null;
      state = {
      modal1Visible: false,
      user:this.props.location.state.data,
      loading:false,
      selectFlag:false,
      screens:[],
      
    }

    constructor(props){
      super(props)
      this.getscreens()
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }


    getscreens = () => {
    axios.get(`${BASE_END_POINT}screen/get`)
    .then(response=>{
      console.log(response.data.data)
      this.setState({screens:response.data.data})
    })
    .catch(error=>{
      //console.log("ALL Categories ERROR")
      //console.log(error.response)
    })
    }

      

      handleSubmit = (e) => {
      e.preventDefault();
      console.log('user ID     ',this.state.user.id)
      this.props.form.validateFields((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          const data = {
            username: values.username,
            screen: values.screen.key,
            type: this.state.user.type,
                                                        
        }
        if(values.password){
          data.password = values.password
        }
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5));           
              this.setState({ modal1Visible:false });
              this.props.history.goBack()
          })
          .catch(error=>{
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
      
    }

    //end submit form
    
    //modal


    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }
      //end modal
      
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
         //select
         const Option = Select.Option;

       
         const {select} = this.props;
 
      return (
          
        <div style={{paddingBottom: '100px'}}>
         <AppMenu height={'200%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#3497fd'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.personalInfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <div class="row">
                           
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.username}>
                            </input>
                            <label for="email" class="active">{allStrings.username}</label>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.id}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.id}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.screen.number}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.screen}</label>
                            </div>
                           
                        </div>
                  

                                
                        <div>
                       
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        </div>
                       
                                                                             
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                            
                            <Form onSubmit={this.handleSubmit} className="login-form">
                            
                            <label for="name" class="lab">{allStrings.username}</label>
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please enter user name' }],
                                initialValue: user.username,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.password}</label>
                            <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: false, message: 'Please enter user name' }],
                            })(
                                <Input/>
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('screen', {
                                rules: [{ required: true, message: 'Please enter screen' }],
                            })(
                              <Select labelInValue  
                              placeholder={allStrings.screen}
                              style={{ width: '100%'}} >
                                  {this.state.screens.map(val=>
                                  <Option value={val.id}>{val.number}</Option>
                                  )}                 
                              </Select>
                            )}
                            </Form.Item>
                          

                            </Form>
                           
                        </Modal>
                             
                    </div>
            </div>
        </div>
        </div>
        </div>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
    getUser,
  }


export default connect(mapToStateProps,mapDispatchToProps) ( AdminInfo = Form.create({ name: 'normal_login' })(AdminInfo)) ;
