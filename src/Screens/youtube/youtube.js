import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';

import './youtube.css';
import { Skeleton, message,Modal, Form,Spin, Icon, Input} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const { TextArea } = Input;


class Video extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCategory:null,
     Video:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    //submit form
    flag = -1;
    getVideo = () => {
      this.setState({loading:true})
      axios.get(`${BASE_END_POINT}video/get`)
      .then(response=>{
        console.log("ALL About")
        console.log(response.data)
       
        this.setState({Video:response.data.data,loading:false})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getVideo()
    }
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           
            const data =
             {
              link:values.link
            }

           
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}video/${this.state.Video[0].id}`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.addDone, 2.5));
                this.setState({ modal1Visible:false });
                this.getVideo()
            })
            .catch(error=>{
              //  console.log(error.response)
                //l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;

           const {select} = this.props;
           
      return (
          <div className='conatiner' style={{paddingBottom: '100px'}}>
            {this.props.currentUser&& this.props.currentUser.user.type =='ADMIN'&&
            
              <AppMenu goTo={this.props.history}></AppMenu>            
            }
            {this.props.currentUser&& this.props.currentUser.user.type =='ADMIN'&&
              <Nav></Nav>            
            }
             
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              
               
               <div className='VideoContainerData'>
               
                    <div className='VideoContainer'>
                          <span className='VideoTitle'>{allStrings.youtube}</span>                       
                    </div>
                    {this.state.loading?
                    <Spin indicator={antIcon} />
                    :
                    <span className='VideoData'>
                      <iframe width="500" height="315"
                      src={this.state.Video[0].link}>
                      </iframe>                       
                      </span>
                    }
               </div>

              {this.props.currentUser&& this.props.currentUser.user.type =='ADMIN'&&
               <button onClick={()=>this.setModal1Visible(true)} className='editVideo'>{allStrings.update}</button>
              }
              
                  
                
              <div>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('link', {
                            rules: [{ required: true, message: 'Please enter Video link' }],
                            initialValue:this.state.loading?'':this.state.Video[0].link
                        })(
                            <TextArea 
                            autosize={{ minRows: 4 }}
                            placeholder={allStrings.link} />
                           
                        )}
                        </Form.Item>

                       
                        
                        
                        
                    </Form>
                </Modal>
              </div> 
            </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)( Video= Form.create({ name: 'normal_login' })(Video));
