import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';

import './show.css';
import { Skeleton, message,Modal, Form,Spin, Icon, Input} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const { TextArea } = Input;


class Show extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    user:[],
    webSite:"",
    time:0
     }
     constructor(props){
      super(props)
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
    componentDidMount(){
      this.getUserData()
      setInterval(()=>{
       this.getUserData()
      },3000)
    }
   
    getUserData = () => {
      axios.get(`${BASE_END_POINT}${this.props.currentUser.user.id}`)
      .then(response=>{
        console.log('user data')
        console.log(response.data.user.screen.time)
        this.setState({user:response.data,time:response.data.user.screen.time})
        console.log(this.state.user)
      })
      .catch(error=>{
        console.log(error.response)
      })
    }
    
    
    
    
    render() {           
      return (
        <div style={{paddingBottom: '0px'}}>
        <iframe src={this.state.user.webSite}
          width="100%" height="655" frameborder="0"
          allowfullscreen sandbox >
        </iframe>        
     </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)( Show= Form.create({ name: 'normal_login' })(Show));
