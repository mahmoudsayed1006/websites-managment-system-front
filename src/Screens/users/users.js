
import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import './users.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,Modal, Form,Select, Icon, Input, Button,  Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//

class User extends React.Component {
 
    constructor(props){
      super(props)
      this.getUsers(1)
      this.getscreens()
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }
   
    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedUser:null,
        users:[],
        loading:true,
        screens:[],
        tablePage:0,
        }

        getscreens = () => {
          axios.get(`${BASE_END_POINT}screen/get`)
          .then(response=>{
            console.log(response.data.data)
            this.setState({screens:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }

      flag = -1;
       getUsers = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}/getAll?type=WORKER&page=${page}&limit={20}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
           console.log("ALL users")
           console.log(response.data.data)
           if(deleteRow){
            this.setState({tablePage:0})
           }
           this.setState({users:deleteRow?response.data.data:[...this.state.users,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL users ERROR")
           //console.log(error.response)
         })
       }
     
       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getUsers(1,true)
             this.flag = -1
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            const data = {
                username: values.username,
                screen: values.screen.key,
                password: values.password,
                type: values.type.key,
                                                           
            }
          
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}signup`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
              console.log(allStrings.addDone)
                l.then(() => message.success('Add User', 2.5));
                this.setState({ modal1Visible:false });
                this.getUsers(1,true)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
              console.log("Add USer Error")
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }
    //end submit form
      


    showModal = () => {
      this.setState({
        visible: true,
      });
      
    }
    

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }


    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }

    validatePhone = (rule, value, callback) => {
      //const { form } = this.props;
      if ( isNaN(value) ) {
        callback('Please enter coorect phone');
      }else if ( value.length <11 ) {
        callback('Phone number must be grater than 11 digit');
      } 
      else {
        callback();
      }
    };
  
  
//end modal//

//end modal

    render() {
      const Option = Select.Option;
      const{select} = this.props
        //form
         const { getFieldDecorator } = this.props.form;
        
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.no}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
        
         let list = this.state.users.map(val=>[val.id,""+val.username,
         " "+val.screen?val.screen.number:' ',controls
        ])
         const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
      return (
          <div style={{paddingBottom: '100px'}}>
              <Nav></Nav>
              <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>

              <div className='menu' style={{marginRight:!select?'20.2%':'5.5%'}}>
              <div style={{height:' 200px',padding: '25px 50px'}}>
                <img style={{width:'250px',height: '150px',borderRadius: '0px'}} src="https://res.cloudinary.com/boody-car/image/upload/v1589791155/s0ovt3jakmi8cive0c7p.png">
                </img>
              </div>
              <Tables 
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.username, allStrings.screen,allStrings.remove]} title={allStrings.userstable}
              arr={this.state.loading?loadingView:list}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta)=>{
                
                if(cellMeta.colIndex!==3){
                  console.log(colData)
                  console.log('row   ',cellMeta.rowIndex+"         "+cellMeta.dataIndex)
                  this.props.history.push('/UserInfo',{data:this.state.users[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===3){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                  }
              }}
 
              
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                 if(currentPage>this.counter){
                   this.counter=currentPage;
                   this.pagentationPage=this.pagentationPage+10
                 }else{
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage-10
                 }
                console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getUsers(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              ></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}}  onClick={() => this.setModal1Visible(true)}>{allStrings.addUser}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                  
                    <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please enter  user Name' }],
                    })(
                        <Input placeholder={allStrings.username} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter  Password' }],
                    })(
                        <Input placeholder={allStrings.password} />
                    )}
                    </Form.Item>

                    <Form.Item>
                    {getFieldDecorator('screen', {
                        rules: [{ required: true, message: 'Please enter screen' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.screen}
                      style={{ width: '100%'}} >
                           {this.state.screens.map(val=>
                           <Option value={val.id}>{val.number}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('type', {
                        rules: [{ required: true, message: 'Please enter user type' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.type}
                      style={{ width: '100%'}} >
                           <Option value='ADMIN'>ADMIN</Option>  
                           <Option value='WORKER'>WORKER</Option>               
                      </Select>
                    )}
                    </Form.Item>
                    
                    </Form>
                </Modal>
              
              </div>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


  export default withRouter( connect(mapToStateProps,mapDispatchToProps)  (User = Form.create({ name: 'normal_login' })(User)) );
 

