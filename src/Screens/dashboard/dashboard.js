import React, { Component } from 'react';
import Nav from '../../components/navbar/navbar';
import AppMenu from '../../components/menu/menu';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import {NavLink} from 'react-router-dom';
import {Skeleton} from 'antd';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {withRouter} from 'react-router-dom'


class Dashboard extends Component {
   
  state = {
    loading:true,
    loading2:true,
    counts:[]
   
}

constructor(props){
    super(props)
    if(this.props.isRTL === true){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }

  }
  
 
    componentDidMount(){
       // console.log(this.props.currentUser)
        console.log("push   ",this.props.history)
    }
      


  render() {
      const loadingView = 
       <Skeleton  active/> 

       const{select} = this.props
       //alert(''+window.innerHeight)
    return (
      <div style={{paddingBottom: '100px'}}>
        <Nav></Nav>
        <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>

        
        <div className='menu'  style={{marginRight:!select?'20.2%':'5.5%'}}>
          <div style={{height:' 200px',padding: '25px 50px'}}>
            <img style={{width:'250px',height: '150px',borderRadius: '0px'}} src="https://res.cloudinary.com/boody-car/image/upload/v1589791155/s0ovt3jakmi8cive0c7p.png">
            </img>
          </div>
          <div className='content' >
            <h1 className='dashTitle'>{allStrings.welcome}</h1>
       
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>group</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.user}</p>
                  </div>
                  </NavLink>
                  </div>         
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/screens'>
                      <div className="icons">
                      <Icon>airplay</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.screens}</p>
                      </div>
                  </NavLink>
                  </div>
                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/Website'>
                      <div className="icons">
                      <Icon>insert_link</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.websites}</p>
                      </div>
                  </NavLink>
                  </div> 
              </div>
          </div>
          </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (Dashboard) );
