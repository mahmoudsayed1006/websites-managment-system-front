import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Login from './login/login';
import UserInfo from './user info/user info'
import Splash from './splash/splash'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import Screen from './screen/screen'
import Youtube from './youtube/youtube'

import ScreenInfo from './screen info/screen info'
import AdminInfo from './admin info/admin info'
import Website from './website/website'
import Show from './show/show'
import WebsiteInfo from './website info/website info'
import Department from './department/department'

import  DepartmentInfo from './department info/department info'
class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App"> 
          <Switch>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route  path='/Login' component={Login}/>
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/screens' component={Screen} />
            <Route path='/ScreenInfo' component={ScreenInfo} />
            <Route path='/AdminInfo' component={AdminInfo} />
            <Route path='/Youtube' component={Youtube} />
            <Route path='/WebsiteInfo' component={WebsiteInfo} />
            <Route path='/Website' component={Website} />
            <Route path='/DepartmentInfo' component={DepartmentInfo} />
            <Route path='/Department' component={Department} />
            <Route path='/Show' component={Show} />
          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
