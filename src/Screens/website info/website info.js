import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import './website info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon,Select} from 'antd';
import {Table} from 'react-materialize'

class WebsiteInfo extends React.Component {
    state = {
        modal1Visible: false,
         Website:this.props.location.state.data,
         file: this.props.location.state.data.img,
         flag:this.props.location.state.flag,
         departments:[],
         }

         constructor(props){
          super(props)
          if(this.props.isRTL=== true){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        showIntervals = () => {
          const flagInterval = 1;
          this.setState({ flagInterval });
        }
        getDepartments = () => {
          axios.get(`${BASE_END_POINT}department/get`)
          .then(response=>{
            console.log(response.data.data)
            this.setState({departments:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }
         componentDidMount()
         {
          this.getDepartments()
         }
   
    
    deleteWebsite = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}website/${this.state.Website.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          
          const data = {
            link: values.link,
            title:values.title,
            department:values.department.key
            
        }
       
        
      
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}Website/${this.state.Website.id}`,data,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {Website} = this.state
        const {select} = this.props;
      return (
          
        <div style={{paddingBottom: '100px'}}>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.Website}</h2>
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Website.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Website.link}>
                            </input>
                            <label for="name" class="active">{allStrings.link}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Website.title}>
                            </input>
                            <label for="name" class="active">{allStrings.title}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Website.department.name}>
                            </input>
                            <label for="name" class="active">{allStrings.department}</label>
                            </div>

                        </div>
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteWebsite}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">


                                  <label for="name" class="lab">{allStrings.Website}</label>
                                
                                  <Form.Item>
                                  {getFieldDecorator('link', {
                                      rules: [{ required: true, message: 'Please enter link' }],
                                      initialValue: Website.link
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('title', {
                                      rules: [{ required: true, message: 'Please enter display name' }],
                                      initialValue: Website.title
                                  })(
                                      <Input placeholder={allStrings.title}/>
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('department', {
                                      rules: [{ required: true, message: 'Please enter department' }],
                                  })(
                                    <Select labelInValue  
                                    placeholder={allStrings.department}
                                    style={{ width: '100%'}} >
                                        {this.state.departments.map(val=>
                                        <Option value={val.id} default>{val.name}</Option>
                                        )}                 
                                    </Select>
                                  )}
                                  </Form.Item>
                               
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(WebsiteInfo = Form.create({ name: 'normal_login', })(WebsiteInfo)) ;
