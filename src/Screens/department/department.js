import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import './department.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Department extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedDepartment:null,
     Departments:[],
     file:null,
     loading:true,
     tablePage:0,
     flagInterval: 0
     }

     constructor(props){
      super(props)
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
    }
    showIntervals = () => {
      const flagInterval = 1;
      this.setState({ flagInterval });
    }

    //submit form
    flag = -1;
    getDepartments = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}department/get?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Departments    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({Departments:deleteRow?response.data.data:[...this.state.Departments,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL Departments ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getDepartments(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteDepartment()
     }

     deleteDepartment = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}department/${this.state.selectedDepartment}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getDepartments(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('date ', date[2]  );
          const data = {
              name: values.name,
              
          }
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}department`,data,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log(allStrings.addDone)
              l.then(() => message.success('Add Department', 2.5));
              this.setState({ modal1Visible:false });
              this.getDepartments(1,true)
              this.flag = -1
              this.props.form.resetFields() 
          })
          .catch(error=>{
            console.log("Add Department Error")
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })

        }
      });
      
    }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list =this.state.Departments.map((val,index)=>[
           val.id,
          val.name,
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
           const { flagInterval} = this.state;
      return (
          <div style={{paddingBottom: '100px'}}>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <div style={{height:' 200px',padding: '25px 50px'}}>
                <img style={{width:'250px',height: '150px',borderRadius: '0px'}} src="https://res.cloudinary.com/boody-car/image/upload/v1589791155/s0ovt3jakmi8cive0c7p.png">
                </img>
              </div>

              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.name,allStrings.remove]} 
              title={allStrings.department}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==2){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/DepartmentInfo',{data:this.state.Departments[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===2){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedDepartment:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getDepartments(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addDepartment}</Button>
              <Modal
                    title={allStrings.add}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                       
                        <Form.Item>
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please enter name' }],
                        })(
                            <Input placeholder={allStrings.name}/>
                        )}
                        </Form.Item>
                       
                    </Form>
                </Modal>
              </div> 
            </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Department = Form.create({ name: 'normal_login' })(Department));
