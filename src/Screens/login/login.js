import React from 'react';
import './login.css';
import {Button,Input} from 'react-materialize';
import {Form} from 'antd'
import "antd/dist/antd.css";
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import {login} from '../../actions/AuthActions'
import  {allStrings} from '../../assets/strings'

class Login extends React.Component {
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.props.login(values,this.props.userToken,this.props.history)
        }
      });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
      return (
        <div class="container" style={{paddingBottom: '100px'}}>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#26a69a'}}>
                    <h2 class="center-align" style={{color:'white'}}>{allStrings.login}</h2>
                </div>
                <div class="row row-form">
                    <form class="col s12">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <row>
                        <Form.Item style={{marginTop:'50px'}}>
                        {getFieldDecorator('username', {
                            rules: [{ required: true, message: 'please enter your username' }],
                        })(
                            <Input style={{textAlign:'right'}} placeholder="username" />
                        )}
                        </Form.Item>
                        <Form.Item style={{marginTop:'50px'}}>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'please enter your password' }],
                        })(
                            <Input type='password' style={{textAlign:'right'}} placeholder="password" />
                        )}
                        </Form.Item>
                    
                    </row>
                   
                        <Form.Item>
                  
                    <row>
                    <br></br>
                    <br></br>
                        <Button type="primary" htmlType="submit" className="login-form-button"style={{color:'white',fontWeight:'500', width:'100%',height:'50px',backgroundColor:'#3497fd'}}>
                           {allStrings.login}
                        </Button>
                    </row>
                        </Form.Item>
                    
                    </Form>
                       
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    userToken: state.auth.userToken,
  })
  
  const mapDispatchToProps = {
    login,
  }


export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Login = Form.create({ name: 'normal_login' })(Login))) ;
