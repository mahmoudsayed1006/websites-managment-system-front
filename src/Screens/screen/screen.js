import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';

import './screen.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm,Select,Radio,Checkbox} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'

let selectedWebsites=[];
let selectedAll=[];
class Screen extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedScreen:null,
     Screens:[],
     file:null,
     loading:true,
     tablePage:0,
     flagInterval: 0,
     websites:[],
     selectedWebsites:[],
     departments:[],
     inputValue:'',
     way:0,
     showWays:0
     }

     constructor(props){
      super(props)
      if(this.props.isRTL === true){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      this.updateInput = this.updateInput.bind(this);
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
    }
    showIntervals = () => {
      const flagInterval = 1;
      this.setState({ flagInterval });
      
    }
    showWays = () => {
      const showWays = 1;
      this.setState({ showWays });
    }
    way1 = () => {
      this.setState({ way:1,flagInterval:0 });
    }
    way2 = () => {
      this.setState({ way:2,flagInterval:1 });
    }
    /*getWebsites = () => {
      axios.get(`${BASE_END_POINT}website/get`)
      .then(response=>{
        console.log(response.data.data)
        this.setState({websites:response.data.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }*/
    getWebsitesFilter = (departmentId) => {
      axios.get(`${BASE_END_POINT}website/get?department=${departmentId}`)
      .then(response=>{
        console.log(response.data.data)
        this.setState({websites:response.data.data})
        let arr =[];
        for(var i=0; i< response.data.data.length;i++){
          arr.push(response.data.data[i].id)
        } 
        selectedAll = arr
        console.log(arr)
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }
    getDepartments = () => {
      axios.get(`${BASE_END_POINT}department/get`)
      .then(response=>{
        console.log(response.data.data)
        this.setState({departments:response.data.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }

    //submit form
    flag = -1;
    getScreens = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}screen/get?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Screens    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({Screens:deleteRow?response.data.data:[...this.state.Screens,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL Screens ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    getscreensfilter = (page,deleteRow,search) => {
      console.log(this.state.inputValue)
      axios.get(`${BASE_END_POINT}screen/get?search=${this.state.inputValue}&page=${page}&limit={20}`)
      .then(response=>{
        console.log("ALL screens")
        console.log(response.data)
        if(deleteRow){
         this.setState({tablePage:0})
        }
        this.setState({Screens:deleteRow?response.data.data:[...this.state.Screens,...response.data.data],loading:false})
        document.getElementById('searchCompany').value = ''
      })
      .catch(error=>{
       this.setState({loading:false})
        console.log("ALL screens ERROR")
        console.log(error.response)
      })
    }
    componentDidMount(){
      this.getScreens(1,true)
      //this.getWebsites()
      this.getDepartments()
    }
    OKBUTTON = (e) => {
      this.deleteScreen()
     }
    handleChange(value) {
      let values = value;
      let arr =[]
      for(var i=0; i< values.length;i++){
        arr.push(values[i].key)
      } 
      selectedWebsites = arr;
      console.log(`selected ${selectedWebsites}`);
    }
    selectAll(e) {
      if(e.target.checked == true){
      selectedWebsites = selectedAll
      }
      console.log(`checked = ${selectedWebsites}`);
    }

     deleteScreen = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}screen/${this.state.selectedScreen}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getScreens(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('date ', date[2]  );
          const data = {
              number: values.number,
              webSite:values.webSite.key,
              departmentId:values.departmentId.key
              
          }
          if(selectedWebsites.length >0){
            data.webSites = selectedWebsites 
          }
          if(values.time){
            data.time = values.time * 60
          }
          
          console.log(data)
         let arr = [];
          if(this.state.flagInterval === 1){
            arr[0] = {from:values.from1,to:values.to1,webSite:values.webSite1.key}
            arr[1] = {from:values.from2,to:values.to2,webSite:values.webSite2.key}
            arr[2] = {from:values.from3,to:values.to3,webSite:values.webSite3.key}
            arr[3] = {from:values.from4,to:values.to4,webSite:values.webSite4.key}
            arr[4] = {from:values.from5,to:values.to5,webSite:values.webSite5.key}
           
          
          }
          if(this.state.flagInterval === 1){
            data.intervals = arr
          }
          
          console.log(JSON.stringify(arr))
          console.log(data)
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}screen`,data,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log(allStrings.addDone)
              l.then(() => message.success('Add Screen', 2.5));
              this.setState({ modal1Visible:false });
              this.getScreens(1,true)
              this.flag = -1
              this.props.form.resetFields() 
          })
          .catch(error=>{
            console.log("Add Screen Error")
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })

        }
      });
      
    }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      updateInput(e){
        this.setState({inputValue : e.target.value})
        }
//end modal
    render() {
        //form
        const Option = Select.Option;
        const{select} = this.props
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let online = (
           <div className='online'/>
         )
         let offline = (
          <div className='offline'/>
        )
         let list =this.state.Screens.map((val,index)=>[
          val.id,val.number,val.webSite.title,val.department,
          val.interval?allStrings.yes:allStrings.no,
          val.online?online:offline,
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const { flagInterval} = this.state;
           
      return (
          <div style={{paddingBottom: '100px'}}>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <div style={{height:' 200px',padding: '25px 50px'}}>
                <img style={{width:'250px',height: '150px',borderRadius: '0px'}} src="https://res.cloudinary.com/boody-car/image/upload/v1589791155/s0ovt3jakmi8cive0c7p.png">
                </img>
              </div>
              <div className='filterCustom'>
              <div class="row">
                
                <div class="input-field col s12" style={{width: '90%', marginRight:'2%'}}>
                  <Input id="searchCompany" style={{width: '90%', marginRight:'2rem',marginLeft:'2rem'}} type='text' name='companyFilter' placeholder={allStrings.search} onChange={this.updateInput}></Input>
                  <Button style={{color: 'white', backgroundColor:'#3497fd', width: '85%', marginRight:'1rem',marginLeft:'1rem',height:'40px'}} onClick={() => this.getscreensfilter(1,`${this.state.inputValue}`)}>{allStrings.search}</Button>
                  <Icon className='controller-icon' type="undo" onClick={() => this.getScreens(1)} />
                </div>
              </div>
              </div>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.number,allStrings.webSite,allStrings.department,allStrings.interval,allStrings.online,allStrings.remove]} 
              title={allStrings.screens}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==6){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/ScreenInfo',{data:this.state.Screens[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===6){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedScreen:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getScreens(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addScreen}</Button>
              <Modal
                    title={allStrings.addScreen}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                       
                        <Form.Item>
                        {getFieldDecorator('number', {
                            rules: [{ required: true, message: 'Please enter arabic Screen' }],
                        })(
                            <Input placeholder={allStrings.number}/>
                        )}
                        </Form.Item>
                       
                        <Form.Item>
                        {getFieldDecorator('departmentId', {
                            rules: [{ required: true, message: 'Please enter department' }],
                        })(
                          <Select labelInValue  
                          placeholder={allStrings.department}
                          style={{ width: '100%'}} 
                          onChange={(value)=>this.getWebsitesFilter(value.key)}
                          >
                              {this.state.departments.map(val=>
                              <Option value={val.id}>{val.name}</Option>
                              )}                 
                          </Select>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('webSite', {
                            rules: [{ required: true, message: 'Please enter web Site' }],
                        })(
                          <Select labelInValue  
                          placeholder={allStrings.webSite}
                          style={{ width: '100%'}} >
                              {this.state.websites.map(val=>
                              <Option value={val.id}>{val.title}</Option>
                              )}                 
                          </Select>
                        )}
                        </Form.Item>
                       
                          <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:0}} onClick={() => this.showWays()}>{allStrings.addIntervals}</Button>
                          <br></br>
                          <br></br>
                          {this.state.showWays==1 &&
                          
                          <Radio.Group name="radiogroup" >
                            <Radio onClick={()=>this.way1()} value={1}>first way</Radio>
                            <Radio onClick={() => this.way2()} value={2}>second way</Radio>
                          
                          </Radio.Group>
                          }
                           { this.state.way==1 &&
                        <div>
                            <Form.Item>
                            {getFieldDecorator('webSites', {
                                rules: [{ required: false, message: 'Please enter web Site' }],
                            })(
                              <Select labelInValue 
                              mode="multiple"
                              onChange={this.handleChange} 
                              placeholder={allStrings.webSite}
                              style={{ width: '100%'}} >
                                  {this.state.websites.map(val=>
                                  <Option value={val.id} key={val.id}>{val.title}</Option>
                                  )}                 
                              </Select>
                            )}
                            </Form.Item>
                            <Checkbox onChange={this.selectAll}>Select all</Checkbox>
                            <Form.Item>
                            {getFieldDecorator('time', {
                                rules: [{ required: true, message: 'Please enter time in minutes' }],
                            })(
                                <Input placeholder={allStrings.time}/>
                            )}
                            </Form.Item>
                            </div>
                        }
                       
              
                        
                        
                          
                          
                          {flagInterval==1 &&
                          <div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from1', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to1', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                              {getFieldDecorator('webSite1', {
                                  rules: [{ required: true, message: 'Please enter web Site' }],
                              })(
                                <Select labelInValue  
                                placeholder={allStrings.webSite}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                              )}
                              </Form.Item>
                              
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from2', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to2', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                              {getFieldDecorator('webSite2', {
                                  rules: [{ required: true, message: 'Please enter web Site' }],
                              })(
                                <Select labelInValue  
                                placeholder={allStrings.webSite}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                              )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from3', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to3', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                              {getFieldDecorator('webSite3', {
                                  rules: [{ required: true, message: 'Please enter web Site' }],
                              })(
                                <Select labelInValue  
                                placeholder={allStrings.webSite}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                              )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from4', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to4', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                              {getFieldDecorator('webSite4', {
                                  rules: [{ required: true, message: 'Please enter web Site' }],
                              })(
                                <Select labelInValue  
                                placeholder={allStrings.webSite}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                              )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from5', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to5', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                              {getFieldDecorator('webSite5', {
                                  rules: [{ required: true, message: 'Please enter web Site' }],
                              })(
                                <Select labelInValue  
                                placeholder={allStrings.webSite}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                              )}
                              </Form.Item>
                            </div>
                          </div>
                          }

                        
                    </Form>
                </Modal>
              </div> 
            </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Screen = Form.create({ name: 'normal_login' })(Screen));
