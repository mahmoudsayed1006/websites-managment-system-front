import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import './screen info.css';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Select,Button,Radio,Checkbox} from 'antd';
import {Table} from 'react-materialize'
let selectedWebsites=[];
let selectedAll=[];
class ScreenInfo extends React.Component {
    state = {
        modal1Visible: false,
         Screen:this.props.location.state.data,
         file: this.props.location.state.data.img,
         flag:this.props.location.state.flag,
         flagInterval:0,
         intervals: this.props.location.state.data,
         websites:[],
         departments:[],
         way:0,
         showWays:0
         }

         constructor(props){
          super(props)
          if(this.props.isRTL=== true){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        showIntervals = () => {
          const flagInterval = 1;
          this.setState({ flagInterval });
          
        }
        showWays = () => {
          const showWays = 1;
          this.setState({ showWays });
        }
        way1 = () => {
          this.setState({ way:1,flagInterval:0 });
        }
        way2 = () => {
          this.setState({ way:2,flagInterval:1 });
        }
         componentDidMount()
         {
          this.getDepartments()
           // this.getWebsites()
         }
         getDepartments = () => {
          axios.get(`${BASE_END_POINT}department/get`)
          .then(response=>{
            console.log(response.data.data)
            this.setState({departments:response.data.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }
    
         /*getWebsites = () => {
            axios.get(`${BASE_END_POINT}website/get`)
            .then(response=>{
              console.log(response.data.data)
              this.setState({websites:response.data.data})
            })
            .catch(error=>{
              //console.log("ALL Categories ERROR")
              //console.log(error.response)
            })
          }*/
          getWebsitesFilter = (departmentId) => {
            axios.get(`${BASE_END_POINT}website/get?department=${departmentId}`)
            .then(response=>{
              console.log(response.data.data)
              this.setState({websites:response.data.data})
              let arr =[];
              for(var i=0; i< response.data.data.length;i++){
                arr.push(response.data.data[i].id)
              } 
              selectedAll = arr
              console.log(arr)
            })
            .catch(error=>{
              //console.log("ALL Categories ERROR")
              //console.log(error.response)
            })
          }
    deleteScreen = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}screen/${this.state.Screen.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
     handleChange(value) {
      let values = value;
      let arr =[]
      for(var i=0; i< values.length;i++){
        arr.push(values[i].key)
      } 
      selectedWebsites = arr;
      console.log(`selected ${selectedWebsites}`);
    }
    selectAll(e) {
      if(e.target.checked == true){
      selectedWebsites = selectedAll
      }
      console.log(`checked = ${selectedWebsites}`);
    }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          
          const data = {
            number: values.number,
            webSite:values.webSite.key,
            departmentId:values.departmentId.key
            
        }
        if(values.department){
          data.department = values.department
        }
        if(selectedWebsites.length >0){
          data.webSites = selectedWebsites 
        }
        if(values.time){
          data.time = values.time * 60
        }
        let arr = [];
        if(this.state.flagInterval === 1){
          arr[0] = {from:values.from1,to:values.to1,webSite:values.webSite1.key}
          arr[1] = {from:values.from2,to:values.to2,webSite:values.webSite2.key}
          arr[2] = {from:values.from3,to:values.to3,webSite:values.webSite3.key}
          arr[3] = {from:values.from4,to:values.to4,webSite:values.webSite4.key}
          arr[4] = {from:values.from5,to:values.to5,webSite:values.webSite5.key}
         
        
        }
        if(this.state.flagInterval === 1){
          data.intervals = arr
        }
        
        console.log(JSON.stringify(arr))
        console.log(data)
      
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}screen/${this.state.Screen.id}`,data,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {Screen} = this.state
         const { flagInterval} = this.state;
        const {select} = this.props;
        const {intervals} = this.state.intervals;
        console.log(intervals)
      return (
          
        <div style={{paddingBottom: '100px'}}>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.screen}</h2>
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Screen.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Screen.number}>
                            </input>
                            <label for="name" class="active">{allStrings.number}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Screen.webSite.link}>
                            </input>
                            <label for="name" class="active">{allStrings.webSite}</label>
                            </div>

                        

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Screen.department}>
                            </input>
                            <label for="name" class="active">{allStrings.department}</label>
                            </div>


                        </div>
                        {Screen.random ==true &&
                          <div>
                            <div class="row">
                              <div class="input-field col s6">
                              <input id="name" type="text" class="validate" disabled value={Screen.time}>
                              </input>
                              <label for="name" class="active">{allStrings.time}</label>
                              </div>
                              </div>
                              <div className='dash-table'>
                                <h5>{allStrings.websites} : </h5>
                                <div className='row'>
                                    <div className="col s6 m6 l6 dashboard-table">
                                        <Table>
                                            <thead>
                                                <tr>
                                                <th data-field="id">{allStrings.title}</th>
                                                <th data-field="name">{allStrings.link}</th>
                                                </tr>
                                            </thead> 

                                            <tbody> 
                                            {Screen.webSites.map(val=>(
                                                <tr>
                                                    <td>{val.title}</td>
                                                    <td>{val.link}</td>                                                                                
                                                </tr>
                                            ))}

                                            </tbody>
                                        </Table>
                                    </div>
                                </div>
                              </div> 
                          </div>
                        }
                        {Screen.interval==true&&
                        <div className='dash-table'>
                            <h5>{allStrings.interval} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.from}</th>
                                            <th data-field="name">{allStrings.to}</th>
                                            <th data-field="arabic">{allStrings.webSite}</th>
                                            </tr>
                                        </thead> 

                                        <tbody> 
                                        {intervals.map(val=>(
                                            <tr>
                                                <td>{val.from}</td>
                                                <td>{val.to}</td>                                 
                                                <td>{val.webSite}</td>
                                                
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
                       }


               
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteScreen}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">


                                  <label for="name" class="lab">{allStrings.Screen}</label>
                                
                                  <Form.Item>
                                  {getFieldDecorator('number', {
                                      rules: [{ required: true, message: 'Please enter arabic Screen' }],
                                      initialValue: Screen.number
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('departmentId', {
                                      rules: [{ required: true, message: 'Please enter department' }],
                                  })(
                                    <Select labelInValue  
                                    placeholder={allStrings.department}
                                    style={{ width: '100%'}} 
                                    onChange={(value)=>this.getWebsitesFilter(value.key)}
                                    >
                                        {this.state.departments.map(val=>
                                        <Option value={val.id}>{val.name}</Option>
                                        )}                 
                                    </Select>
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                    {getFieldDecorator('webSite', {
                                        rules: [{ required: true, message: 'Please enter web Site' }],
                                    })(
                                    <Select labelInValue  
                                    placeholder={Screen.webSite.title}
                                    style={{ width: '100%'}} >
                                        {this.state.websites.map(val=>
                                        <Option value={val.id}>{val.title}</Option>
                                        )}                 
                                    </Select>
                                    )}
                                  </Form.Item>
                                  
                                  <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:0}} onClick={() => this.showWays()}>{allStrings.addIntervals}</Button>
                                  <br></br>
                                  <br></br>
                                  {this.state.showWays==1 &&
                                  
                                  <Radio.Group name="radiogroup" >
                                    <Radio onClick={()=>this.way1()} value={1}>first way</Radio>
                                    <Radio onClick={() => this.way2()} value={2}>second way</Radio>
                                  
                                  </Radio.Group>
                                  }
                                  { this.state.way==1 &&
                                <div>
                                    <Form.Item>
                                    {getFieldDecorator('webSites', {
                                        rules: [{ required: false, message: 'Please enter web Site' }],
                                    })(
                                      <Select labelInValue 
                                      mode="multiple"
                                      onChange={this.handleChange} 
                                      placeholder={allStrings.webSite}
                                      style={{ width: '100%'}} >
                                          {this.state.websites.map(val=>
                                          <Option value={val.id} key={val.id}>{val.title}</Option>
                                          )}                 
                                      </Select>
                                    )}
                                    </Form.Item>
                                    <Checkbox onChange={this.selectAll}>Select all</Checkbox>
                                    <Form.Item>
                                    {getFieldDecorator('time', {
                                        rules: [{ required: true, message: 'Please enter time in minutes' }],
                                    })(
                                        <Input placeholder={allStrings.time}/>
                                    )}
                                    </Form.Item>
                                    </div>
                                }
                              
                      
                                
                                
                                  
                          
                                  {flagInterval==1&&
                          <div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from1', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to1', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                                {getFieldDecorator('webSite1', {
                                    rules: [{ required: true, message: 'Please enter web Site' }],
                                })(
                                <Select labelInValue  
                                placeholder={Screen.webSite.link}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                                )}
                              </Form.Item>
                             
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from2', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to2', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                                {getFieldDecorator('webSite2', {
                                    rules: [{ required: true, message: 'Please enter web Site' }],
                                })(
                                <Select labelInValue  
                                placeholder={Screen.webSite.link}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                                )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from3', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to3', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                                {getFieldDecorator('webSite3', {
                                    rules: [{ required: true, message: 'Please enter web Site' }],
                                })(
                                <Select labelInValue  
                                placeholder={Screen.webSite.link}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                                )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from4', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to4', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                                {getFieldDecorator('webSite4', {
                                    rules: [{ required: true, message: 'Please enter web Site' }],
                                })(
                                <Select labelInValue  
                                placeholder={Screen.webSite.link}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                                )}
                              </Form.Item>
                            </div>
                            <div style={{padding:0,  display:'flex', width:'100%',flexDirection:this.props.isRTL?'row-reverse':'row'}}>
                              <Form.Item>
                              {getFieldDecorator('from5', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0, width:130,marginRight:10,marginLeft:0}} placeholder={allStrings.from} />
                              )}
                              </Form.Item>

                              <Form.Item>
                              {getFieldDecorator('to5', {
                                  rules: [{ required: true, message: 'require' }],
                              })(
                                  <Input style={{marginBottom:0,width:130,marginRight:10,marginLeft:10}}  placeholder={allStrings.to} />
                              )}
                              </Form.Item>
                              <Form.Item>
                                {getFieldDecorator('webSite5', {
                                    rules: [{ required: true, message: 'Please enter web Site' }],
                                })(
                                <Select labelInValue  
                                placeholder={Screen.webSite.link}
                                style={{marginBottom:0,width:250,marginRight:10,marginLeft:10}}>
                                    {this.state.websites.map(val=>
                                    <Option value={val.title}>{val.title}</Option>
                                    )}                 
                                </Select>
                                )}
                              </Form.Item>
                            </div>
                          </div>
                          }
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(ScreenInfo = Form.create({ name: 'normal_login', })(ScreenInfo)) ;
